function removeDuplicates(input) {
    var hashObject = new Object();
     
    for (var i = input.length - 1; i >= 0; i--) {
        var currentItem = input[i]; 
     
        if (hashObject[currentItem] == true) {
            input.splice(i, 1);
        }
         
        hashObject[currentItem] = true;
    }
    return input;
}

function getKeyboardNumbers() {
    var keyboard = window.frames[0].document.getElementsByClassName('btnTeclaNumberOn');

    var availableDigits = new Array();

    for (var key in keyboard) {
      if (keyboard.hasOwnProperty(key)) {
        var input = keyboard[key];
        if(input.value != undefined) {
            var inputValue = input.value.replace(/[^0-9+]/g, '');

            availableDigits.push(inputValue);
        }
      }
    }

    return removeDuplicates(availableDigits);
}

function triggerEvent(event, element) {
    var e = new Event(event);
    element.addEventListener(event);
    element.dispatchEvent(e);
}

function findNumberInInput(num) {
    var keyboardDigits = getKeyboardNumbers();

    if (typeof num != "string") {
        num = num.toString();
    }

    for (var l = 0; l < num.length; l++) {
        var digit = num.charAt(l);

        for (var i = 0; i < keyboardDigits.length; i++) {
            if (digit == keyboardDigits[i].charAt(0) ||
                digit == keyboardDigits[i].charAt(1)) {

                var inputIndex = i + 1;
                var selectedInput = window.frames[0].document.getElementById('btnTeclaNumPar'.concat(inputIndex));

                console.log('selected: '+selectedInput.value);
                triggerEvent('click', selectedInput);
                triggerEvent('mousedown', selectedInput);
            }
       }         
    }
}

function findInputWithValue(inputs, value) {
    var i = inputs.length;
    while (i--) {
        if (inputs[i].value === value) {
			console.log('Found letter: ' + inputs[i].value);
            triggerEvent('click', inputs[i]);
        }
    }
}

function enterMyNumberPassBuddy(numberPass) {
    var num;

    for (var n = 0; n < numberPass.length; n++) {
        num = numberPass.charAt(n);

        window.setTimeout(findNumberInInput, 900 * (n+1), num);
    }
}

function isSpaceChar(c) {
	return (c == ' ');
}

function enterMyPhrasePassBuddy(phrasePass) {
	phrasePass = phrasePass.toUpperCase();

	var phraseInput = window.frames[0].document.getElementsByName('btnTeclaAlfa');

	for (var p = 0; p < phrasePass.length; p++) {
		letter = phrasePass.charAt(p);

		if (isSpaceChar(letter)) {
			letter = 'Espaço';
		}

		window.setTimeout(findInputWithValue, 400 * (p+1), phraseInput, letter);
	}
}

function runToTheHugMoQuirido() {
	var submitButton = window.frames[0].document.getElementById('btnConfirmar');
	
	triggerEvent('click', submitButton);
}



